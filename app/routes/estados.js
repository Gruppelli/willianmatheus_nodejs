var connectionFactory = require('../infra/connectionFactory');

module.exports = function(app){

	app.get('/estados', function(req, res){

		var connection = connectionFactory();
		
		connection.query('select * from estados', function(erro, resposta){

			res.render('estado/lista', {list: resposta});

		});

		connection.end();

	});

}